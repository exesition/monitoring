# 4. Подготовка cистемы мониторинга

## Подготовка:

Для работы решил использовать [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus)

Качаем репозиторий к себе нa `master node` и далее следуем инструкциям в заголовке Quickstart

```bash
kubectl apply --server-side -f manifests/setup
kubectl wait --for condition=Established --all CustomResourceDefinition --namespace=monitoring
kubectl apply -f manifests/
kubectl -n monitoring delete networkpolicies.networking.k8s.io --all
```
Для получения доступа к Grafana меняем настройки Service
```bash
kubectl --namespace monitoring patch svc grafana -p '{"spec": {"type": "NodePort"}}'
```

<p align="left">
  <img src="./img/04_01_grafana.png">
</p>

<p align="left">
  <img src="./img/04_02_grafanadash.png">
</p>

<p align="left">
  <img src="./img/04_02_grafanapods.png">
</p>

Доступ к графане

[Grafana](http://158.160.158.194:32168/)<br>

```text
логин admin
пароль diplom_app
```

Далее необходимо развернуть тестовое приложение в кластере для этого переключаемся на namespace `stage`

```bash
kubectl config set-context --current --namespace=stage
```
Проверяем пространства имён:
```bash
NAME              STATUS   AGE
default           Active   153m
kube-node-lease   Active   153m
kube-public       Active   153m
kube-system       Active   153m
monitoring        Active   41m
stage             Active   48m
```

Создаём файл ./stage/deploy.yml:
```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: diplom-app
  name: diplom-app
  namespace: stage
spec:
  replicas: 1
  selector:
    matchLabels:
      app: diplom-app
  template:
    metadata:
      labels:
        app: diplom-app
    spec:
      containers:
        - image: exesition/diplom_app:v1.0.0
          imagePullPolicy: IfNotPresent
          name: diplom-app
      terminationGracePeriodSeconds: 30
      
---
apiVersion: v1
kind: Service
metadata:
  name: diplom-app
  namespace: stage
spec:
  ports:
    - name: web
      port: 80
      targetPort: 80
      nodePort: 30080
  selector:
    app: diplom-app
  type: NodePort
```

Запускаем установку deployment в Kubernetes:

```bash
kubectl apply -f deploy/stage/deploy.yml
deployment.apps/diplom-app created
service/diplom-app created
```

Проверяем pods:
```bash
kubectl get deploy,po,svc -n stage
NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/diplom-app   1/1     1            1           104s

NAME                            READY   STATUS    RESTARTS   AGE
pod/diplom-app-64d84bf8-s4b4b   1/1     Running   0          104s

NAME                 TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
service/diplom-app   NodePort   10.106.155.19   <none>        80:30080/TCP   104s
```

Настраиваем доступ к нашему приложению. Создаём файл `ingress.yaml`
```yaml
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: diplom-app
  namespace: stage
spec:
  rules:
  - http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: diplom-app
            port:
              number: 80
```

Запускаем установку `ingress` в Kubernetes:
```bash
kubectl apply -f deploy/stage/ingress.yml
ingress.networking.k8s.io/diplom-app created
```

После этого можем заходить на любую из рабочих нод.<br>
[84.201.152.15](http://84.201.152.15:30080)<br>
[62.84.116.11](http://62.84.116.11:30080)<br>
[158.160.158.194](http://158.160.158.194:30080)<br>


<p align="left">
  <img src="./img/04_03_app.png">
</p>